# Yiddish keyboard layouts by Okey_Kompyuter / אַ ייִדעשע קלאַויאטורס פֿון אָקײ_קאָמפּיוטער.

Cross-platform Yiddish keyboard layouts. Currently supporting a QWERTY-based OSX layout. Windows 10 (and above) and Linux versions to come.

## Installation

### macOS 

1. Download [ok_k-yiddish-klaviatur.dmg](https://gitlab.com/okey_kompyuter/yiddish-keys-osx/-/raw/master/ok_k-yiddish-klaviatur.dmg?inline=false), mount it (double click on the downloaded file and wait for a pop up to appear), drag the yod to the Keyboard Layouts folder.

2. Open up your Keyboard Prefences.

  * If you're already using multiple layouts you can just click on your current layout in the taskbar and then select 'Open Keyboard Preferences' from the drop-down menu.
  * If you're not already using multiple layouts, press ⌘ and spacebar together and type 'keyboard'. Hit enter, then click the `Input Sources` tab.

3. In Keyboard Preferences, click the + icon in the bottom-left. Search for Yiddish, and then 'Yiddish - QWERTY'. Click it and then Add.

4. To switch to the Yiddish keyboard, in the taskbar (top of the screen), next to the date and time, you should see a keyboard selector. Click this. 'Yiddish - QWERTY' should be listed. Select and you're done! 

## Layout Rules (in order of priority)

1. One letter, one key, one utf-8 glyph. No repetition.
   - Unicode supports the full Yiddish character set, including װ, ױ,ײ,פֿ. One key press should place a single UTF-8 character, and not multiple individual characters.
2. Phonetic placement of letters takes priority, where possible
   - אַ -> a
   - אָ -> o
   - ב -> b
   - ג -> g
   - ה -> h
   - ק -> k
   - פּ -> p
   - פֿ -> f
   - י -> i
   - ע -> e
   - ק -> k
   - װ -> v
   - ײ -> y
3. Aesthetic similarity when phonetic layout not possible
   - ש -> w
   - כ -> c
   - צ -> x
4. Functional placement where neither phonetic or aesthetic placement possible
   - א -> j (easy to access)
5. Final, variant and holy letters access via shift
   - וּ -> shift-u
   - יִ -> shift-i
   - ם -> shift-m
   - ן -> shift-n
   - ך -> shift-c
   - ף -> shift-f
   - ח -> shift-h
   - ת -> shift-s
   - תּ -> shift-t
   - שׂ -> shift-w
   - בֿ -> shift-v & shift-b (an exception to the one-letter one-key rule since both are equally reasonable placements)
6. Yiddish-native punctuation
   - ־ -> -
   - ׳ (gereysh) -> '
   - „ (open quote) -> "
   - “ (close quote) -> q
   - ״ (tzvey gereyshn) -> shift-q
7. Currencies and symbols relevant to global Yiddish world (not just America!)
   - $ -> shift-4
   - £ -> shift-3
   - ₪ -> shift-1
   - zł -> alt-z
   - ¥ -> alt-y ([Why Japanese Yen?](http://www.kanji.org/kanji/jack/yiddish/jyc.htm))
8. Caps switches to English


## Hey dumdum, hey schmo

### Didn't you know you can write Yiddish with a Hebrew layout?**


Like a frozen forest bare of birds and leaves, Yiddish orthography on the internet 
has shed its defining hats, dots and strikes because of the lack of alternatives 
to Modern Hebrew layouts, for which all such things are superfluous. Not only that, 
but Hebrew's prioritization of different letters to Yiddish means that typing is so
frustrating for many that they prefer typing in Latin characters than Hebrew. 
Transliteration hurts the language - it prevents the development of litteracy, 
especially for those who come to the language as students rather than through birth.

This Yiddish keyboard hopes to address this by providing an intuitive and eventually
standard Yiddish keyboard for all major OSs to ultimately, it is hoped, include 
as vanilla feature included by Apple with their operating system.

## Acknowledgements 

This keyboard layout was made possible thanks to the marvelous [Ukelele](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=ukelele), which you can use to edit the keyboard layout.


## Licence

This is distributed under a [GPL3](https://www.gnu.org/licenses/gpl-3.0.en.html) free software licence.

http://www.shretl.org
